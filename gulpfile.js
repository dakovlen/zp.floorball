var gulp = require('gulp'),
    sass = require('gulp-sass'),
    rigger = require('gulp-rigger'),
    autoprefixer = require('gulp-autoprefixer');

var autoprefixerOptions = {
    browsers: ['last 2 versions', '> 5%', 'Firefox ESR']
};

var concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify');

var vendor = [
    'node_modules/jquery/dist/jquery.min.js',
    'node_modules/wow.js/dist/wow.js',
    //'node_modules/lightgallery/dist/js/lightgallery.js',
    //'node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.js',
    'node_modules/slick-carousel/slick/slick.js',
    'libs/**/*.js'
];

//LIBS-SCRIPTS
gulp.task('libs-scripts', function() {
    gulp.src(vendor)
        .pipe(concat('libs-scripts.js'))
        .pipe(gulp.dest('public/js'))
        .pipe(rename('libs-scripts.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('public/js'));
});

// CSS
gulp.task('scss', function() {
    gulp.src('resources/sass/style.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(autoprefixer(autoprefixerOptions))
        .pipe(rename('style.min.css'))
        .pipe(gulp.dest('public/css'));
});

// JS
gulp.task('scripts', function() {
    gulp.src('resources/js/**/*.js')
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest('public/js'))
        .pipe(rename('scripts.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('public/js'));
});

// HTML
gulp.task('html', function () {
    gulp.src('resources/html/**/*html')
        .pipe(rigger())
        .pipe(gulp.dest('public/'))
});

// IMAGES
gulp.task('images', function() {
    gulp.src('resources/images/**/*.+(png|jpg|gif|svg|ico)')
        .pipe(gulp.dest('public/images'))
});

// VIDEO
gulp.task('video', function() {
    gulp.src('resources/video/**/*.+(mp4|webm|avi|mov|flv|wmv)')
        .pipe(gulp.dest('public/video'))
});

// FONTS
gulp.task('fonts', function() {
    gulp.src('resources/fonts/**/*.+(json|eot|svg|ttf|woff)')
        .pipe(gulp.dest('public/fonts'))
});

// DEFAULT
gulp.task('default', function() {
    gulp.run("images");
    gulp.run("video");
    gulp.run('fonts');
    gulp.run("scss");
    gulp.run("scripts");
    gulp.run("libs-scripts");
    gulp.run("html");

    gulp.watch('resources/fonts/**/*.+(json|eot|svg|ttf|woff)', function(event) {
        gulp.run('fonts')
    });

    gulp.watch('resources/images/**/*.+(png|jpg|gif|svg|ico)', function(event){
        gulp.run('images');
    });

    gulp.watch('resources/video/**/.+(mp4|webm|avi|mov|flv|wmv)', function(event){
        gulp.run('video');
    });

    gulp.watch('resources/html/**/*.html', function(event) {
        gulp.run('html');
    });

    gulp.watch('resources/sass/**/*.scss', function(event) {
        gulp.run('scss');
    });

    gulp.watch('resources/js/**/*.js', function(event) {
        gulp.run('scripts');
    });
});
